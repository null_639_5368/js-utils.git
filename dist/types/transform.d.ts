/**
 * blobToDataURL
 * @param blob
 * @returns
 */
export declare const blobToDataURL: (blob: Blob) => Promise<unknown>;
/**
 * urlToBase64
 * @param imgUrl
 * @returns
 */
export declare const urlToBase64: (imgUrl: any) => Promise<unknown>;
export declare const xhrequest: (url: any) => Promise<unknown>;
/**
 * 文件压缩
 * @author maybe
 * @param {File | Blob | url} source 转入对象类型 File 或 Blob 或 http链接
 * @param {*} option  compressorjs 配置
 * @returns {Object{base64:string,file:File}}
 */
export declare const fileCompressor: (source: any, option: any) => Promise<unknown>;
