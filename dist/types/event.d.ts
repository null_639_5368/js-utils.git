/**
* 判断是否在元素外触发事件
* @param event  事件源
* @param el 元素
* @returns {*} true 在元素外 false 在元素内
*/
export declare function isOutEl(event: Event, el: HTMLElement): boolean;
/**
* 获取事件冒泡路径
* @description 兼容ie11,edge,chrome,firefox,safari
* @param evt
* @returns {Array}
*/
export declare function getEventPath(evt: any): any;
/**
 * 获取移动端和pc端触摸（pc端鼠标模拟）事件名对象
 */
export declare function getTouchEventNameMap(): {
    touchStart: string;
    touchMove: string;
    touchEnd: string;
};
