/**
* 存储localStorage
* @param name
* @param content
*/
export declare function setStore(name: string, content: any): void;
/**
* 获取localStorage
* @param name
* @returns {string}
*/
export declare function getStore(name: string): string;
/**
* 删除localStorage
* @param name
*/
export declare function removeStore(name: string): void;
