/**
* 动态创建script
* @param {string} src 脚本地址
* @param {string} id 唯一标识
* @param {'defer' | 'async'} load 'defer' | 'async'  加载脚本的时机,默认是同步加载
* @example
* createScript(...).then().catch()
* ----or----
* try {await  createScript(...)} catch(err){console.log(err)}
* @returns {Promise<any>} Promise
*/
export declare function createScript(src: string, id?: string, load?: 'defer' | 'async'): Promise<any>;
/**
* 删除脚本
* @param {HTMLElement} dom script元素
* @returns {Promise<any>} Promise
*/
export declare function removeScript(dom: HTMLElement): Promise<any>;
